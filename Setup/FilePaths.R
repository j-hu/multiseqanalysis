## FILE LOCATIONS & SETUP ------------------------------------------------------

# What did you name the CellRanger output?
analysis_name <- "LTR"
# Which CR lanes do you want to process? Type the lane numbers: list(1,2,3,4)
CR_lanes <- list(1, 2, 3, 4)
# Do you want to make a CITEseq whitelist?
make_whitelist = FALSE

n_lanes <- length(CR_lanes)
lanes_str <- paste0("lane", paste(CR_lanes, collapse = "")) # string: lane1234

drive <- "/Volumes/Vasudha_4" # Mac
# drive <- "E:" # Windows
drive_dir <- file.path(drive, "MULTIseq_Oct_2018")
save_dir <- file.path(drive_dir, "Datafiles", analysis_name)
img_dir <- file.path(drive_dir, "Plots", analysis_name)
bulk_dir <- file.path(drive, "Bulk2017")
# create directories if they do not exist yet
foo <- lapply(c(save_dir, img_dir), FUN = function(x) {
  if (!dir.exists(x)) { dir.create(x, recursive = TRUE) } })
rm(foo)



# Please place folders from outs/raw_gene_bc_matrices in corresponding folders:
CR_dirs <- sapply(1:4, FUN = function(x) {
  paste0(drive_dir, "CellRanger/crHMEC_outs/HMEC", x,
         "/raw_gene_bc_matrices/hg19_", analysis_name, "/") })

files <- list(metadata = file.path(drive_dir, "Datafiles", "metadata_rd1.csv"),
              CITE_table = file.path(drive_dir, "Datafiles", "CS_tables.rds"),
              CITEs = file.path(drive_dir, "CSCount", "CSCount111518_HMEC", 1:4, ".tsv"),
              cells_all_list = file.path(save_dir, paste0("cells_all_list_", lanes_str, ".rds")),
              hmec_filtered_list = file.path(save_dir, paste0("u2filtered_list_",
                                                lanes_str, ".rds")),
              hmec_2000_list = file.path(save_dir, paste0("u2filtered_list_",
                                            lanes_str, ".rds")),
              hmec_2000 = file.path(save_dir, paste0("u2filtered_combined_",
                                       lanes_str, ".rds")),
              whitelist = file.path(save_dir, paste0(analysis_name, "_whitelist.txt")),
              CRCS = file.path(save_dir, paste0("which_CS_", lanes_str, ".rds")),
              final_calls = file.path(save_dir, paste0("final_calls_", lanes_str, ".rds")),
              CR_classes =  file.path(save_dir, paste0("CR_calls_", lanes_str, ".rds")),
              hmec_filtered = file.path(save_dir, paste0("u2filtered_", lanes_str, ".rds")),
              hmec_classed = file.path(save_dir, paste0("classified_", lanes_str, ".rds")),
              seu_hmec = file.path(save_dir, paste0("seu_hmec_", lanes_str, ".rds")),
              seu_singlet = file.path(save_dir, paste0("seu_singlet_", lanes_str, ".rds")),
              seu_singlet_imputed = file.path(save_dir, paste0("seu_singlet_imputed_", lanes_str, ".rds")),
              subsets = file.path(save_dir, paste0(lanes_str, "_subsets.rds")),
              seuTG = file.path(save_dir, paste0(lanes_str, "_seu7u5gt2TG.rds")),
              seu_sub = file.path(save_dir, paste0(lanes_str, "_seu7cut0.3TGG1.rds")))
