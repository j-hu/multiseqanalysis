## Analysis Functions
#   Jennifer Hu
#   
#   Basic Utilities
#     list_bind(l, by)                                Uses (1: rbind, 2: cbind) to combine elements of l.
#     append_lane(bc_list, lane)                      If no number already present, appends lane.
#   Seurat-Related Functions
#     nVarGenes(seu)                                  Number of genes in seu@var.genes.
#     seurat_subset(seu, cells)                       Subsets seu by cells, then normalizes, scales, var.genes.
#     seurat_subset_by(seu, viruses, idents, meta_by, meta_val, bool, which_cells)
#     PCA_tSNE(seu, dims.embed)                       Runs PCA and tSNE on seu. 2D tSNE by default.
#     assign_clusters(seu, ID, assignments)           Assigns @meta.data$ID according to @ident assignments.
#     rename_untr(seu)                                Replaces all "" in @meta.data$virus with "untr".
#     attach_metadata(seu, calls, metadata_file)      Adds LMO classifications and metadata from file to seu.
#     plot_pMito_nUMI_nGene(seu)                      Two-panel plot of nUMI/pMito and nUMI/nGene
#   Seurat Analysis
#     fraction_above(seu, fetch, above, meta)         What % of parent meta group has fetch > above?
#   File I/O
#     save_png(p, filename, h, w)                     Saves PNG at filename, size h x w. Returns p.
#     save_object(object, dir, lanes_str)             Saves object_lanes_str.rds in dir/Quicksave.


# #   Basic Utilities ----------------------------------------------

## list_bind(l, by)
# Binds the objects in the list together by = 1 rows or 2 columns. Max of 4 elements.
# I dunno why R's built-in rbind and cbind won't take lists as arguments.
list_bind = function(l, by) {
  n <- length(l)
  if (n == 1) { return(l[[1]]) }
  if (by == 1) { # rbind
    if (n == 2) { return(rbind(l[[1]], l[[2]])) }
    if (n == 3) { return(rbind(l[[1]], l[[2]], l[[3]])) }
    if (n == 4) { return(rbind(l[[1]], l[[2]], l[[3]], l[[4]])) }
  } else {
    if (n == 2) { return(cbind(l[[1]], l[[2]])) }
    if (n == 3) { return(cbind(l[[1]], l[[2]], l[[3]])) }
    if (n == 4) { return(cbind(l[[1]], l[[2]], l[[3]], l[[4]])) }
  }
  if (n > 4) { warning("list_bind only works for up to 4 elements."); return(l) }
}

## append_lane(bc_list, lane)
# First checks if lane number has already been added, then adds lane.
# Raises warning if existing lane is different from lane.
append_lane = function(bc_list, lane) {
  require(stringr)
  lapply(bc_list, FUN = function(x) { 
    # Check that lane number hasn't yet been appended
    len <- str_length(x)
    last <- suppressWarnings(as.numeric(substr(x, len, len)))
    if (is.na(last)) {
      # Rename cell barcodes to include CellRanger Lane number
      return( paste0(x, lane) )
    } else { 
      if (last != lane) { warning("Mismatched lane assignment!") }
      return(x)
    }
  })
}

## GOcluster(markers, cluster_name, prefix)
# Using the markers provided, looks for the ones associated with cluster at power > 0.4
# Saves results as separate csv files. Returns enriched list of dataframes.
GOcluster = function(markers, cluster_name, prefix) {
  markers <- markers[markers$cluster == cluster_name, ]
  markers_up = markers[markers$avg_diff > 0 & markers$power > 0.4, "gene"]
  markers_down = markers[markers$avg_diff < 0 & markers$power > 0.4, "gene"]
  stopifnot(length(intersect(markers_up, markers_down)) == 0)
  dbs <- listEnrichrDbs()
  GOdbs <- dbs$libraryName[startsWith(dbs$libraryName, "GO_") & endsWith(dbs$libraryName, "_2018")]
  enriched <- list("up" = enrichr(markers_up, GOdbs), "down" = enrichr(markers_down, GOdbs))
  # save them
  write.csv(enriched[["up"]]$GO_Biological_Process_2018, 
            file.path(save_dir, "GO", "Biological Process", paste0(prefix, cluster_name, "_upBiologicalProcess.csv")))
  write.csv(enriched[["up"]]$GO_Cellular_Component_2018, 
            file.path(save_dir, "GO", "Cellular Component", paste0(prefix, cluster_name, "_upCellularComponent.csv")))
  write.csv(enriched[["up"]]$GO_Molecular_Function_2018, 
            file.path(save_dir, "GO", "Molecular Function", paste0(prefix, cluster_name, "_upMolecularFunction.csv")))
  write.csv(enriched[["down"]]$GO_Biological_Process_2018, 
            file.path(save_dir, "GO", "Biological Process", paste0(prefix, cluster_name, "_downBiologicalProcess.csv")))
  write.csv(enriched[["down"]]$GO_Cellular_Component_2018, 
            file.path(save_dir, "GO", "cellular Component", paste0(prefix, cluster_name, "_downCellularComponent.csv")))
  write.csv(enriched[["down"]]$GO_Molecular_Function_2018, 
            file.path(save_dir, "GO", "Molecular Function", paste0(prefix, cluster_name, "_downMolecularFunction.csv")))
  cat("GO up/down files saved in", file.path(save_dir, "GO"), "folder for", prefix, cluster_name, "\n")
  return(enriched)
}

# #  File I/O -----------------------------------------------------

## save_png(p, filename, h, w)
# Returns the plot, which can be passed to print() for display
save_png = function(p, filename, h, w) {
  png(filename = filename, height = h, width = w)
  print(p)
  dev.off()
  return(p)
}

## save_object(object, dir, lanes_str)
# Quick way to save a variable. Makes a file in dir with the name of the object, appends lanes_str.
save_object = function(object, dir, lanes_str) {
  qs_dir <- file.path(dir, "Quicksave")
  if (!dir.exists(qs_dir)) { dir.create(qs_dir, recursive = TRUE) }
  saveRDS(object, paste0(qs_dir, "/", deparse(substitute(object)), "_", lanes_str, ".rds"))
}

## v.as.character(v)
# Vectorized as.character.
v.as.character = function(v) {
  sapply(v, as.character)
}

# #  Seurat-Related Functions -------------------------------------

## nVarGenes(seu)
nVarGenes <- function(seu) {
  length(seu@var.genes)
}

## seurat_subset(seu, cellnames, norm, scale, vargenes)
# Normalize, scale, and find variable genes for each new Seurat object subset based on cellIDs
seurat_subset <- function(seu, cellnames, norm = TRUE, scale = TRUE, vargenes = TRUE) {
  seu_sub <- SubsetData(seu, cells.use = cellnames)
  if (norm) {
    cat("\nLog-normalizing.")
    seu_sub <- NormalizeData(seu_sub, display.progress = FALSE)
  }
  if (scale) {
    cat("\nScaling by nUMI and pMito.")
    seu_sub <- ScaleData(seu_sub, vars.to.regress = c("nUMI", "PercentMito"), display.progress = FALSE)
  }
  if (vargenes) {
    seu_sub <- FindVariableGenes(seu_sub, x.low.cutoff = 0.025, y.cutoff = 0.65, do.plot = FALSE, display.progress = FALSE)
    # Check nVarGenes
    cat("\n", nVarGenes(seu_sub), "variable genes found.\n")
  }
  return(seu_sub)
}

## seurat_subset_by(seu, viruses, idents, meta_by, meta_vals, bool, which_cells, norm, scale, vargenes)
# Creates new Seurat object (normalized etc)
seurat_subset_by <- function(seu, viruses = NULL, timepoint = NULL, specID = NULL, idents = NULL, 
                             meta_by = NULL, meta_vals = NULL, bool = NULL, which_cells = NULL,
                             norm = TRUE, scale = TRUE, vargenes = TRUE) {
  # start with all TRUE
  cells <- matrix(TRUE, nrow = length(seu@cell.names), ncol = 6)
  if (!is.null(viruses)) {
    cells[,1] <- (seu@meta.data$virus %in% viruses)
  }
  if (!is.null(timepoint)) {
    cells[,2] <- (seu@meta.data$timepoint == timepoint)
  }
  if (!is.null(specID)) {
    cells[,2] <- (seu@meta.data$specimenID == specID)
  }
  if (!is.null(idents)) {
    cells[,4] <- (seu@ident %in% idents)
  }
  if (!is.null(meta_by) & !is.null(meta_vals)) {
    cells[,5] <- (seu@meta.data[, meta_by] %in% meta_vals)
  }
  if (!is.null(bool)) {
    cells[,6] <- bool
  }
  if (!is.null(which_cells)) {
    # find cell names shared by logical index and which_cells
    cellnames <- intersect(which_cells,
                           seu@cell.names[which(cells[, 1] & cells[, 2] & cells[, 3] & 
                                                  cells[, 4] & cells[, 5] & cells[, 6])])
  } else {
    cellnames <- seu@cell.names[which(cells[, 1] & cells[, 2] & cells[, 3] & 
                                        cells[, 4] & cells[, 5] & cells[, 6])]
  }
  return(seurat_subset(seu, cellnames, norm = norm, scale = scale, vargenes = vargenes))
}

## PCA_tSNE(seu)
# Runs default PCA and tSNE analysis using pc.genes = seu@var.genes, dims.use=1:10, resolution = 0.5
PCA_tSNE <- function(seu, dims.embed = 2) {
  seu <- RunPCA(seu, pc.genes = seu@var.genes, pcs.print = 0)
  seu <- RunTSNE(seu, dims.use = 1:10, verbose = FALSE, dims.embed = dims.embed)
  seu <- FindClusters(seu, genes.use = seu@var.genes, dims.use = 1:10, resolution = 0.5)
  return(seu)
}

## assign_clusters(seu, ID, assignments)
# Creates new metadata column "ID" from seu@ident, based on list of assignments
# EXAMPLE: assignments = list("foo" = c(1, 2, 4), "bar" = c(3, 5, 6))
assign_clusters <- function(seu, ID, assignments) {
  # must be arranged exactly as rownames == seu@cell.names, so get direct from seu
  df <- data.frame(cluster = seu@ident, row.names = seu@cell.names)
  # define "translator" function that looks in assignments
  assigned <- apply(df, 1, FUN = function(x) {
    for (cluster_name in names(assignments)) {
      if (x %in% assignments[[cluster_name]]) { return(cluster_name) }
    }
    # if you get to here, no cluster was found
    return(NA)
  })
  seu <- AddMetaData(object = seu, metadata = assigned, col.name = ID)
  TSNEPlot(seu, group.by = ID, do.label = TRUE)
  return(seu)
}

## rename_untr(seu)
# Kinda dumb. Empty string isn't a good idea since it sometimes gets turned into a variable.
rename_untr <- function(seu) {
  levels(seu@meta.data$virus)[levels(seu@meta.data$virus)==""] <- "untr"
  return(seu)
}

## attach_metadata(seu, calls, metadata_file)
# Given calls and metadata_file containing "BC_ID" column, adds columns of seu@meta.data merged on BC_ID
# Adds metadata column lane based on the last number in cellname
attach_metadata = function(seu, calls, metadata_file) {
  require(stringr)
  stopifnot(length(seu@cell.names) == length(calls))
  # create BC_ID and SingletDoublet metadata columns
  seu@meta.data$BC_ID <- calls[seu@cell.names]
  seu@meta.data$SingletDoublet <- seu@meta.data$BC_ID
  seu@meta.data$SingletDoublet[which(seu@meta.data$BC_ID != "Doublet")] <- "Singlet"
  # create lane metadata column
  seu@meta.data$lane <- sapply(seu@cell.names, FUN = function (x) {
    len <- str_length(x)
    last <- suppressWarnings(as.numeric(substr(x, len, len)))
  })
  # load metadata from file
  metadata <- read.csv(metadata_file, header = TRUE, stringsAsFactors=TRUE)
  # must be arranged exactly as rownames == seu@cell.names
  mdf <- data.frame(BC_ID = seu@meta.data$BC_ID)
  # use join from plyr to maintain row order of mdf! Don't use a different function!
  mdf <- plyr::join(mdf, metadata, by="BC_ID", type="left")
  # since row order is kept, I can add rownames directly
  rownames(mdf) <- seu@cell.names
  # Add HMEC metadata to Seurat object
  seu <- AddMetaData(object=seu, metadata=mdf)
  # Make sure it worked (assertion testing)
  singlets <- which(seu@meta.data$SingletDoublet == "Singlet")
  stopifnot(all(is.na(seu@meta.data[which(seu@meta.data$BC_ID == "Doublet"),"date"])))
  stopifnot(!any(is.na(seu@meta.data[singlets,"Sequence"])))
  return(seu)
}

## plot_pMito_nUMI_nGene(seu)
plot_pMito_nUMI_nGene = function(seu) {
  par(mfrow = c(1, 2))
  GenePlot(object = seu, gene1 = "nUMI", gene2 = "PercentMito", cex.use = 0.05)
  GenePlot(object = seu, gene1 = "nUMI", gene2 = "nGene", cex.use = 0.05)
}

# # Seurat Analysis ----------------------------------------------

## fraction_above(seu, meta, fetch, use.imputed, use.scaled, use.raw)
# Subsets the data to only cells with [fetch] above [above],
# then counts the number in each [meta] category. Compares count to parent.
fraction_above = function(seu, meta, fetch, above, use.imputed = FALSE,
                          use.scaled = FALSE, use.raw = FALSE) {
  table(seu@meta.data[WhichCells(seu, subset.name = fetch, accept.low = above,
                                 use.imputed = use.imputed, use.scaled = use.scaled, use.raw = use.raw), 
                      meta])/
    table(seu@meta.data[, meta])
}

## get_markers(seu)
get_markers = function(seu) {
  markers <- FindAllMarkers(seu, thresh.test = 3, print.bar = FALSE, test.use = "roc")
  markers_use <- subset(markers, avg_diff > 0 & power > 0.4)$gene
  markers_use <- markers_use[markers_use != "LTR"]
}

## plot_subgroups(seu, group, subgroup)
# Plots bar graphs (fractional) that show the proportions of subgroups within groups.
# The group and subgroup are the names of columns in meta.data.
plot_subgroups = function(seu, group, subgroup) {
  # needs to be characters because later used as names
  groups <- v.as.character(unique(seu@meta.data[, group]))
  subgroups <- unique(seu@meta.data[, subgroup])
  stopifnot(!any(is.na(groups))) # sorry
  counts <- sapply(groups, FUN = function(x) {
    table(seu@meta.data[seu@meta.data[[group]] == x, subgroup]) 
  })
  names(counts) <- groups
  melted <- melt(counts)
  # weird, column order in melt is not fixed? manually determine column name
  meltnames <- c("", "", "")
  for (i in 1:ncol(melted)) {
    values <- unique(melted[i])
    if (all(sapply(values, FUN = function (x) { x %in% groups }))) {
      meltnames[i] <- group
    } else if (all(sapply(values, FUN = function (x) { x %in% subgroups }))) {
      meltnames[i] <- subgroup
    } else if (colnames(melted)[i] == "value") {
      meltnames[i] <- "count"
    }
  }
  colnames(melted) <- meltnames
  p <- ggplot(data = melted, aes_string(x = group, y = "count", fill = subgroup)) +
    geom_bar(position = "fill", stat = "identity") + 
    labs(x = group, y = "Fraction", title = paste(subgroup, "Composition by", group))
  return(x90(p))
}

## x90(p)
# Rotates the x axis labels to 90 degrees.
x90 = function(p) {
  return(p + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)))
}
