# classification_functions.r
# All the functions used for LMO barcode classification of CITEseq output in one place.
# This way, just source("classification_functions.r") once.
#   localMaxima(x)
#   findThresh(call.list, id)
#   classifyCells(data, q)
#   plot_sweep(results, extrema)

localMaxima <- function(x) {
  # Use -Inf instead if x is numeric (non-integer)
  y <- diff(c(-.Machine$integer.max, x)) > 0L
  rle(y)$lengths
  y <- cumsum(rle(y)$lengths)
  y <- y[seq.int(1L, length(y), 2L)]
  if (x[[1]] == x[[2]]) {
    y <- y[-1]
  }
  y
}
findThresh <- function(call.list, id) {
  require(reshape2)
  res <- as.data.frame(matrix(0L, nrow=length(call.list), ncol=4))
  colnames(res) <- c("q","pDoublet","pNegative","pSinglet")
  q.range <- unlist(strsplit(names(call.list), split="q="))
  res$q <- as.numeric(q.range[grep("0", q.range)])
  nCell <- length(call.list[[1]])
  for (i in 1:nrow(res)) {
    temp <- table(call.list[[i]])
    if ( "Doublet" %in% names(temp) == TRUE ) { res$pDoublet[i] <- temp[which(names(temp) == "Doublet")] }
    if ( "Negative" %in% names(temp) == TRUE ) { res$pNegative[i] <- temp[which(names(temp) == "Negative")] }
    res$pSinglet[i] <- sum(temp[which(names(temp) != "Doublet" & names(temp) != "Negative")])
  }
  res <- melt(res, id.vars="q")
  res[,4] <- res$value/nCell
  colnames(res)[2:4] <- c("Subset","nCells","Proportion")
  assign(x=paste("res_",id,sep=""), value=res, envir = .GlobalEnv)
  extrema <- res$q[localMaxima(res$Proportion[which(res$Subset == "pSinglet")])]
  assign(x=paste("extrema_",id,sep=""), value=extrema, envir = .GlobalEnv)
}

## data is output file for barcode library
## Row is Cells, Column is LMO barcodes
classifyCells <- function(data, q) {
  require(KernSmooth)
  # Normalize Data: Log2 Transform, replace -Infs, mean-center
  data.n <- as.data.frame(log2(data))
  for (i in 1:ncol(data)) {
    ind <- which(is.finite(data.n[,i]) == FALSE)
    data.n[ind,i] <- 0 # Replace -Inf with 0
    data.n[,i] <- data.n[,i]-mean(data.n[,i]) # Center to column mean
  }
  n_bc <- ncol(data.n)
  n_cells <- nrow(data.n)
  # the names of barcodes associated with cells
  bc_calls <- rep("Negative",n_cells)
  names(bc_calls) <- rownames(data.n)
  
  ## Generate Thresholds: Gaussian KDE with bad barcode detection, outlier trimming
  ## local maxima estimation with bad barcode detection, threshold definition and adjustment
  # iterate over each LMO barcode (columns)
  for (i in 1:n_bc) {
    # Binned Kernel Density Estimate:
    model <- tryCatch( { approxfun(bkde(data.n[,i], kernel="normal")) },
                       error=function(e) { cat(paste0("No threshold found for ", colnames(data.n)[i],"...\n")) } )
    if (class(model) == "character") { next }
    x <- seq(from=quantile(data.n[,i],0.001), to=quantile(data.n[,i],0.999), length.out=100)
    extremum <- localMaxima(model(x))
    if (length(extremum) <= 1) {
      cat(paste0("No threshold found for ", colnames(data.n)[i],"...\n"))
      next
    }
    low.extremum <- min(extremum)
    high.extremum <- max(extremum)
    thresh <- (x[high.extremum] + x[low.extremum])/2
    low.extrema <- extremum[which(x[extremum] <= thresh)]
    new.low <- low.extrema[which.max(model(x)[low.extrema])]
    thresh <- quantile(c(x[high.extremum], x[new.low]), q)
    # find which cells have a value for that barcode > thresh
    cell_i <- which(data.n[,i] >= thresh)
    n <- length(cell_i)
    if (n == 0) { next } # no cells
    # add barcode to every element of bc_calls that needs it
    bc_calls[cell_i] <- sapply(bc_calls[cell_i], FUN = function(x) {
      if (x == "Negative") { return(colnames(data.n)[i]) } else { return("Doublet") }
    })
  }
  return(bc_calls)
}

plot_sweep <- function(results, extrema) {
  require(ggplot2)
  p <- ggplot(data=results, aes(x=q, y=Proportion, color=Subset)) + geom_line() + 
    geom_vline(xintercept=extrema, lty=2) + scale_color_manual(values=c("red","black","blue")) + 
    ggtitle(extrema)
  return(p)
}

## readCSC(CITE_file)
# Returns bar.table with 96 columns corresponding to barcodes.
readCSC = function(CITE_file) {
  # Read in and format Cite-Seq Count output
  bar.table <- read.csv(CITE_file, header=TRUE, row.names=1, stringsAsFactors=FALSE)
  bar.table <- t(bar.table)
  bar.table <- bar.table[,1:96]
  return(bar.table)
}

## classification(cells.final, name = "", bar.table)
# Returns list with bar.table and calls. Optional name input for naming resulting plots/files.
# If you already loaded CITEseq data into a bar.table, add it as an argument because it's faster
classification = function(cells_to_classify, name="", bar.table, drive_dir = NULL) {
  save_plots_files <- !is.null(drive_dir)
  if (save_plots_files) { 
    classplot_dir <- paste0(drive_dir, "Plots/Classification/")
    if (!dir.exists(classplot_dir)) { dir.create(classplot_dir, recursive = TRUE) }
  }
  if (name != "") { name <- paste0(name,"_") }
  cat(paste0("Initial CITEseq count: ", nrow(bar.table), "\n"))
  
  # Subset CSCount output according to cells present in expression library
  ind <- (rownames(bar.table) %in% cells_to_classify)
  # 49408 unduplicated cells with >2000 UMIs present in CSCount output matrix
  if (!all(ind)) { bar.table <- bar.table[ind, ] }
  cat(paste0("Unique cells with >= 2000 UMI: ", nrow(bar.table), "\n"))
  
  ## Sweeps
  qs <- seq(0.01, 0.99, by=0.02)
  sweep_list <- vector("list", length=length(qs))
  neg.cells <- vector("list", length=10)
  calls <- vector("list", length=10) # that's probably enough iterations
  n_neg <- 1 # initialize to be non-zero

  for (i in 1:10) {
    # update bar.table
    if (i > 1) {
      bar.table <- bar.table[-which(rownames(bar.table) %in% neg.cells[[i-1]]), ]
    }
    cat(paste0("Round ",i," beginning with ",nrow(bar.table)," cells.\n"))
    j <- 0
    for (q in qs) {
      j <- j + 1
      sweep_list[[j]] <- classifyCells(data = bar.table, q=q)
      names(sweep_list)[j] <- paste0("q=",q)
    }
    findThresh(call.list = sweep_list, id="0")
    p <- plot_sweep(results=res_0, extrema=extrema_0) + ggtitle(paste("Round",i,":",extrema_0))
    print(p)
    if (save_plots_files) {
      # save plot
      save_png(p, filename = paste0(classplot_dir, name, "round", i, ".png"), 
               w = 700, h = 600)
    }
    # check for multiple extrema
    if (length(extrema_0) == 1) {
      e <- extrema_0
    } else {
      # automatically assign real extremum by Proportion
      prop <- res_0[res_0$Subset == "pSinglet" & res_0$q %in% extrema_0, c("q", "Proportion")]
      e <- res_0[which.max(res_0$Proportion), "q"]
    }
    calls[[i]] <- sweep_list[[paste0("q=",e)]]
    neg.cells[[i]] <- names(calls[[i]])[which(calls[[i]] == "Negative")]
    n_neg <- length(neg.cells[[i]])
    cat(paste0("Identified ",n_neg," negative cells in round ",i," using q=",e,".\n"))
    
    if (n_neg < 1) {
      # exit this loop when done
      break
    }
  }
  calls <- calls[1:i]
  if (save_plots_files) {
    saveRDS(unlist(neg.cells), file.path(drive_dir, "Datafiles", paste0(name, "neg_cells.rds")))
    saveRDS(calls, file.path(drive_dir, "Datafiles", paste0(name, "calls.rds")))
  }
  # return final bar.table and calls
  return(list("bar.table" = bar.table, "calls" = calls[[i]]))
}

## plot_LMOs(bar.table)
# Saves 96 pngs in Plots/analysis_name/LMOs/ directory, prepended by name_ (or nothing), returns singlet/doublet plot
plot_LMOs = function(bar.table, final.calls, name = "", img_dir) {
  if (nrow(bar.table) != length(final.calls)) {
    cat("Inconsistent number of cells.\n")
    return(NA)
  }
  require("Rtsne")
  lmo_dir <- file.path(img_dir, "LMOs")
  if (!dir.exists(lmo_dir)) { dir.create(lmo_dir, recursive = TRUE) }
  
  bar.table.n <- as.data.frame(log2(bar.table))
  for (i in 1:ncol(bar.table.n)) {
    ind <- which(is.finite(bar.table.n[,i]) == FALSE)
    bar.table.n[ind,i] <- 0
    bar.table.n[,i] <- bar.table.n[,i]-mean(bar.table.n[,i])
  }
  bar.tsne <- Rtsne(bar.table.n, dims = 2, initial_dims = 96, verbose = TRUE, 
                    check_duplicates = FALSE, max_iter = 2500)
  temp <- as.data.frame(bar.tsne$Y); colnames(temp) <- c("TSNE1","TSNE2")
  temp[,3:98] <- bar.table.n
  temp[,"Classification"] <- final.calls
  
  # saves all of the plots as png in /Plots/LMOs/name_##.png
  if (name != "") { name <- paste0(name,"_") }
  for (LMO in colnames(bar.table.n)) {
    print(save_png(p = ggplot(data = temp, aes_string(x = "TSNE1", y = "TSNE2", color = LMO)) +
                     geom_point() + scale_color_gradient(low = "black", high = "red") +
                     ggtitle(LMO) + theme(legend.position = "none"),
                   filename = file.path(lmo_dir, paste0(name, LMO, ".png")),
                   w = 500, h = 450))
  }
  
  # color by singlet/doublet
  ggplot(data = temp, aes(x = TSNE1, y = TSNE2, color = Classification)) + geom_point() +
    scale_color_manual(values = c(rep("black", 96), "red")) + theme(legend.position = "none")
}