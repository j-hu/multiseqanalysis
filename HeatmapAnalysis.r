# Dendrograms, Heatmaps, and Marker analysis, with GO term enrichment
seu_sub <- readRDS(files$seu_sub)

## Look for marker genes of AMB cluster -----------------------------------------
# make a thing to hold markers
markers <- list("Lineage" = c(), "LEP_TG" = c(), "MEP_TG" = c(), "LEP20_TG" = c(), "MEP20_TG" = c(),
                "LEP_TGg" = c(), "MEP_TGg" = c(), "LEP20_TGg" = c(), "MEP20_TGg" = c())

seu_sub <- SetAllIdent(seu_sub, "Lineage")
markers[["Lineage"]] <- FindAllMarkers(seu_sub, thresh.test = 3, test.use = "roc")
markers_use = subset(markers[["Lineage"]], avg_diff > 0 & power > 0.4)$gene
print(save_png(p = DoHeatmap(seu_sub, genes.use = markers_use, slim.col.label = TRUE, remove.key = TRUE,
                             title = "LEP / MEP Heatmap"),
               filename = file.path(img_dir, "Supervised Clustering", "Heatmap Lineage sub.png"),
               w = 900, h = 1200))

enriched <- GOcluster(markers[["Lineage"]], "LEP")
enriched <- GOcluster(markers[["Lineage"]], "MEP")

## Create dendrograms using Transgene as Identifier --------------------------------
seu_sub <- SetAllIdent(seu_sub, "Transgene")

for (lineage in c("LEP", "MEP")) {
  cat(lineage, "-------------------------\n")
  lcells <- WhichCells(seu_sub, subset.name = "Lineage", accept.value = lineage)
  seu_sub_lin <- seurat_subset(seu_sub, cellnames = lcells, norm = FALSE, scale = FALSE)
  seu_sub_lin@var.genes <- seu_sub_lin@var.genes[seu_sub_lin@var.genes != "LTR"]
  transgenes <- c("GFP", "cAkt1", "KRAS", "cyclinD1", "Ecadsh1", "Her2", "YAP", "PIK3CAH1047R", "p16sh_PIK3CAH1047R",
                  "MYC", "p16sh_MYC", "p53R175H")
  transgenes <- c("GFP", "PIK3CAH1047R", "p53R175H")
  topcells <- c()
  # Find top 20 cells for each transgene
  for (tg in transgenes) {
    topcells <- c(topcells, tail(names(sort(seu_sub_lin@scale.data["LTR", seu_sub_lin@meta.data$Transgene == tg])), 20))
  }
  seu_sub_20 <- seurat_subset(seu_sub_lin, cellnames = topcells, norm = FALSE, scale = FALSE)
  seu_sub_20@var.genes <- seu_sub_20@var.genes[seu_sub_20@var.genes != "LTR"]
  seu_sub_20 <- RunPCA(seu_sub_20, pc.genes = seu_sub_20@var.genes, pcs.print = 0)
  print(save_png(p = PCAPlot(seu_sub_20, group.by = "virus", do.return = TRUE, title = paste(lineage, "Top 20 PCA")),
                 filename = file.path(img_dir, paste0("PCA 7top20TGG1 small.png")),
                 w = 700, h = 500))

  seu_sub_lin <- BuildClusterTree(seu_sub_lin, genes.use = seu_sub_lin@var.genes, show.progress = FALSE)

  # put markers in
  markers[[paste0(lineage, "_TG")]] <- FindAllMarkers(seu_sub_lin, thresh.test = 3,
                                                      test.use = "roc", print.bar = FALSE)
  markers_use <- subset(markers[[paste0(lineage, "_TG")]], avg_diff > 0 & power > 0.4)$gene
  markers_use <- markers_use[markers_use != "LTR"]
  # make heatmap
  print(save_png(p = DoHeatmap(seu_sub_lin, genes.use = markers_use, slim.col.label = TRUE, remove.key = TRUE,
                               title = paste(lineage, "Heatmap by Transgene"), do.plot = TRUE),
                 filename = file.path(img_dir, "Supervised Clustering",
                                      paste0("Heatmap Transgene sub_", lineage,".png")),
                 w = 900, h = 1200))
  # make GO term csv
  enriched <- lapply(unique(markers[[paste0(lineage, "_TG")]]$cluster),
                     FUN = function (x) { GOcluster(markers[[paste0(lineage, "_TG")]], x, paste0(lineage, "_")) })
  # repeat for top 20
  markers[[paste0(lineage, "20_TG")]] <- FindAllMarkers(seu_sub_20, thresh.test = 3, print.bar = FALSE,
                                                        test.use = "roc")
  markers_use <- subset(markers[[paste0(lineage, "20_TG")]], avg_diff > 0 & power > 0.4)$gene
  markers_use <- markers_use[markers_use != "LTR"]
  print(save_png(p = DoHeatmap(seu_sub_20, genes.use = markers_use, slim.col.label = TRUE, remove.key = TRUE,
                               title = paste(lineage, "Heatmap by Transgene, top20small"), do.plot = TRUE),
                 filename = file.path(img_dir, "Supervised Clustering",
                                      paste0("Heatmap Transgene sub_", lineage,"_top20small.png")),
                 w = 900, h = 1200))
  enriched <- lapply(transgenes,
                     FUN = function (x) { GOcluster(markers[[paste0(lineage, "20_TG")]], x, paste0(lineage, "_top20small_")) })
}

## Create new subgroups based on dendrograms ------------------------------
TG_groups <- list("A" = c("GFP", "PTENsh1", "untr"),
                  "B" = "Twist1", "C" = "cyclinD1", 
                  "D" = c("p16sh_PIK3CAH1047R", "KRAS", "cAkt1", "PIK3CAH1047R"),
                  "E" = "Ecadsh1", "F" = c("p16sh", "p53R175H"), 
                  "G" = "YAP", "H" = "Her2", "I" = c("MYC", "p16sh_MYC"))
seu_G1TGg <- assign_clusters(seu_sub, "TG.Group", TG_groups)

# make dendrograms using these new identities
seu_G1TGg <- SetAllIdent(seu_G1TGg, "TG.Group")
seu_G1TGg_split = list("LEP" = c(), "MEP" = c(), "LEP20" = c(), "MEP20" = c())

for (lineage in c("LEP", "MEP")) {
  # Separate seu_G1TGg into LEP / MEP Seurat objects
  seu_G1TGg_split[[lineage]] <- seurat_subset_by(seu_G1TGg, meta_by = "Lineage", meta_vals = lineage)
  
  # Remove any categories with fewer than 20 cells
  l20 <- paste0(lineage, 20)
  seu_G1TGg_split[[l20]] <- seurat_subset_by(seu_G1TGg_split[[lineage]], meta_by = "TG.Group",
                                             meta_vals = names(table(seu_G1TGg_split[[lineage]]@meta.data$TG.Group))[
                                               table(seu_G1TGg_split[[lineage]]@meta.data$TG.Group) > 20], 
                                             scale = FALSE, norm = FALSE)
  
  seu_G1TGg_split[[lineage]] <- BuildClusterTree(seu_G1TGg_split[[lineage]], genes.use = seu_G1TGg_split[[lineage]]@var.genes)
  seu_G1TGg_split[[l20]] <- BuildClusterTree(seu_G1TGg_split[[l20]], genes.use = seu_G1TGg_split[[l20]]@var.genes)
  
  
}

## Identify markers of Identities, make heatmaps ---------------
seus_heatmap <- seu_G1TGg_split

table(seus_heatmap[["LEP"]]@meta.data$TG.Group)
markers[["LEP_TGg"]] = FindAllMarkers(seus_heatmap[["LEP"]], thresh.test = 3, test.use = "roc")
markers_use = subset(markers[["LEP_TGg"]], avg_diff > 0 & power > 0.4)$gene
DoHeatmap(seus_heatmap[["LEP"]], genes.use = markers_use, 
          slim.col.label = TRUE, remove.key = TRUE)

markers[["LEP20_TGg"]] = FindAllMarkers(seus_heatmap[["LEP20"]], thresh.test = 3, test.use = "roc")
markers_use = subset(markers[["LEP20_TGg"]], avg_diff > 0 & power > 0.4)$gene
DoHeatmap(seus_heatmap[["LEP20"]], genes.use = markers_use, 
          slim.col.label = TRUE, remove.key = TRUE)

table(seus_heatmap[["MEP"]]@meta.data$TG.Group)
markers[["MEP_TGg"]] = FindAllMarkers(seus_heatmap[["MEP"]], thresh.test = 3, test.use = "roc")
markers_use = subset(markers[["MEP_TGg"]], avg_diff > 0 & power > 0.4)$gene
DoHeatmap(seus_heatmap[["MEP"]], genes.use = markers_use, 
          slim.col.label = TRUE, remove.key = TRUE)

markers[["MEP20_TGg"]] = FindAllMarkers(seus_heatmap[["MEP20"]], thresh.test = 3, test.use = "roc")
markers_use = subset(markers[["MEP20_TGg"]], avg_diff > 0 & power > 0.4)$gene
DoHeatmap(seus_heatmap[["MEP20"]], genes.use = markers_use, 
          slim.col.label = TRUE, remove.key = TRUE)

## Make GO enrichment files -----------------------------------------------------------
enriched <- lapply(unique(markers[["LEP_TG"]]$cluster), FUN = function (x) { GOcluster(markers[["LEP_TG"]], x) })
enriched <- lapply(unique(markers[["LEP_TGg"]]$cluster), FUN = function (x) { GOcluster(markers[["LEP_TGg"]], x) })
enriched <- lapply(unique(markers[["MEP_TG"]]$cluster), FUN = function (x) { GOcluster(markers[["MEP_TG"]], x) })
enriched <- lapply(unique(markers[["MEP_TGg"]]$cluster), FUN = function (x) { GOcluster(markers[["MEP_TGg"]], x) })
